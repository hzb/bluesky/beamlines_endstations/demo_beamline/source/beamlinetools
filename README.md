**beamlinetools** 

Python library of tools used at BESSY II, for the container deployment of bluesky

**Installation**

To install pull this repository and then run 

`python3 -m pip install -e .`

the flag -e mens that the package will be installed in editable mode, so that changes will be available without re-installing the package